package com.lara;

import org.apache.log4j.Logger;

public class BootStrap {
	final static Logger Log = Logger.getLogger(BootStrap.class);
	public static void main(String[] args) {
		System.out.println("Hello");
		Log.info("info");
		Log.trace("trace");
		Log.debug("debug");
		Log.error("error");
		Log.warn("warn");
		Log.fatal("fatal");

	}

}
